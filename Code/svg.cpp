// @licstart revoropt
// This file is part of Revoropt, a library for the computation and 
// optimization of restricted Voronoi diagrams.
//
// Copyright (C) 2013 Vincent Nivoliers <vincent.nivoliers@univ-lyon1.fr>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
// @licend revoropt
#include "svg.hpp"

#include <iostream>
#include <fstream>
#include <algorithm>

void SvgPad::solid_point( int x, int y) {
  file_ << "<circle "
        << "cx=\"" << x << "\" "
        << "cy=\"" << y << "\" "
        << "r=\"8\" "
        << "fill=\"rgb(" << fill_[0] << ", " << fill_[1] << ", " << fill_[2] << ")\" "
        << "/>" << std::endl ;
}
void SvgPad::solid_point( double x, double y) {
  solid_point( scalex(x), scaley(y)) ;
}

void SvgPad::contour_point( int x, int y ) {
  file_ << "<circle "
        << "cx=\"" << x << "\" "
        << "cy=\"" << y << "\" "
        << "r=\"12\" "
        << "stroke=\"rgb(" << stroke_[0] << ", " << stroke_[1] << ", " << stroke_[2] << ")\" "
        << "stroke-width=\"2\" "
        << "style=\"fill:none\""
        << "/>" << std::endl ;
}
void SvgPad::contour_point( double x, double y ) {
  contour_point( scalex(x), scaley(y) ) ;
}

void SvgPad::line( int x1, int y1, int x2, int y2 ) {
  file_ << "<line "
        << "x1=\"" << x1 << "\" y1=\"" << y1 << "\" "
        << "x2=\"" << x2 << "\" y2=\"" << y2 << "\" "
        << "stroke=\"rgb(" << stroke_[0] << ", " << stroke_[1] << ", " << stroke_[2] << ")\" "
        << "stroke-width=\"2\""
        << "/>" << std::endl ;
}
void SvgPad::line( double x1, double y1, double x2, double y2 ) {
  line( scalex(x1), scaley(y1), scalex(x2), scaley(y2) ) ;
}

void SvgPad::triangle( int x1, int y1, int x2, int y2, int x3, int y3 ) {
  file_ << "<polygon "
        << "points=\"" 
        << x1 << "," << y1 << " "
        << x2 << "," << y2 << " "
        << x3 << "," << y3 << "\" "
        << "fill=\"rgb(" << fill_[0] << ", " << fill_[1] << ", " << fill_[2] << ")\" "
        << "stroke=\"rgb(" << stroke_[0] << ", " << stroke_[1] << ", " << stroke_[2] << ")\" "
        << "stroke-width=\"2\""
        << "/>" << std::endl ;
}
void SvgPad::triangle( double x1, double y1, double x2, double y2, double x3, double y3 ) {
  triangle( scalex(x1), scaley(y1), scalex(x2), scaley(y2), scalex(x3), scaley(y3) ) ;
}

void SvgPad::polygon( const int* vertices, unsigned int size ) {
  file_ << "<polygon "
        << "points=\"" ;
  for(unsigned int i = 0; i < size; ++i) {
    file_ << vertices[2*i] << "," << vertices[2*i+1] << " " ;
  }
  set_fill_stroke() ;
  file_ << "/>" << std::endl ;
}
void SvgPad::polygon( const double* vertices, unsigned int size) {
  file_ << "<polygon "
        << "points=\"" ;
  for(unsigned int i = 0; i < size; ++i) {
    file_ << scalex(vertices[2*i]) << "," << scaley(vertices[2*i+1]) << " " ;
  }
  set_fill_stroke() ;
  file_ << "/>" << std::endl ;
}

void SvgPad::box( int minx, int miny, int maxx, int maxy ) {
  file_ << "<polygon "
        << "points=\"" 
        << minx << "," << miny << " "
        << maxx << "," << miny << " "
        << maxx << "," << maxy << " "
        << minx << "," << maxy << "\" "
        << "fill=\"rgb(" << fill_[0] << ", " << fill_[1] << ", " << fill_[2] << ")\" "
        << "stroke=\"rgb(" << stroke_[0] << ", " << stroke_[1] << ", " << stroke_[2] << ")\" "
        << "stroke-width=\"2\""
        << "/>" << std::endl ;
}
void SvgPad::box( double minx, double miny, double maxx, double maxy ) {
  box( scalex(minx), scaley(miny), scalex(maxx), scaley(maxy) ) ;
}

int SvgPad::scalex( double x ) {
  int margin = width_ * 5 / 100 ;
  return (int) margin + x*(width_ - 2*margin) ;
}

int SvgPad::scaley( double x ) {
  int margin = height_ * 5 / 100 ;
  return (int) margin + (1-x)*(height_ - 2*margin) ;
}

int SvgPad::scale_byte( double v ) {
  return std::min((int) (v * 256), 255) ;
}

void SvgPad::fill(int r, int g, int b) {
  fill_[0] = r ;
  fill_[1] = g ;
  fill_[2] = b ;
}

void SvgPad::fill(const int color[3]) {
  std::copy(color, color + 3, fill_) ;
}

void SvgPad::fill(double r, double g, double b) {
  fill(scale_byte(r), scale_byte(g), scale_byte(b)) ;
}

void SvgPad::fill(const double color[3]) {
  fill(scale_byte(color[0]), scale_byte(color[1]), scale_byte(color[2])) ;
}

void SvgPad::toggle_fill() {
  use_fill_ = !use_fill_ ;
}

void SvgPad::stroke(int r, int g, int b) {
  stroke_[0] = r ;
  stroke_[1] = g ;
  stroke_[2] = b ;
}

void SvgPad::stroke(const int color[3]) {
  std::copy(color, color + 3, stroke_) ;
}

void SvgPad::stroke(double r, double g, double b) {
  stroke(scale_byte(r), scale_byte(g), scale_byte(b)) ;
}

void SvgPad::stroke(const double color[3]) {
  stroke(scale_byte(color[0]), scale_byte(color[1]), scale_byte(color[2])) ;
}

void SvgPad::toggle_stroke() {
  use_stroke_ = !use_stroke_ ;
}

void SvgPad::set_fill_stroke() {
  file_ << "fill=\"" ;
  if(use_fill_) {
    file_ << "rgb("
          << fill_[0] << ", "
          << fill_[1] << ", "
          << fill_[2] << ")" ;
  } else {
    file_ << "none" ;
  }
  file_ << "\" stroke=\"" ;
  if(use_stroke_) {
    file_ << "rgb("
          << stroke_[0] << ", "
          << stroke_[1] << ", "
          << stroke_[2] << ")" ;
  } else {
    file_ << "none" ;
  }
  file_ << "\"" ;
}

void SvgPad::open() {
  file_ << "<svg height=\"" << height_ 
        << "\" width=\"" << width_ 
        << "\">" << std::endl ;
}

void SvgPad::close() {
  file_ << "</svg>" << std::endl ;
}
