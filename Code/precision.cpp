#include <boost/numeric/interval.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <eigen3/Eigen/Dense>
#include <random>
#include <algorithm>
#include <iostream>

//types
using interval = boost::numeric::interval<double> ;
using rational = boost::multiprecision::cpp_rational ;
using MatDX = Eigen::MatrixXd ;
using VecDX = Eigen::VectorXd ;
using MatIX = Eigen::Matrix<interval, Eigen::Dynamic, Eigen::Dynamic> ;
using VecIX = Eigen::Matrix<interval, Eigen::Dynamic, 1> ;
using MatRX = Eigen::Matrix<rational, Eigen::Dynamic, Eigen::Dynamic> ;
using VecRX = Eigen::Matrix<rational, Eigen::Dynamic, 1> ;
  
//fix problems when using intervals in Eigen
namespace std {
  template<typename X, typename S, typename P>
  struct is_convertible<X,boost::numeric::interval<S,P> > {
    enum { value = is_convertible<X,S>::value };
  };

  template<typename S, typename P1, typename P2>
  struct is_convertible<boost::numeric::interval<S,P1>,boost::numeric::interval<S,P2> > {
    enum { value = true };
  };
}

//{{{ command line

//get the token immediatly after an option given by name
char* get_cmd_option( 
    char ** begin, char ** end, 
    const std::string & option
    ) 
{
  //look for the desired option
  char ** itr = std::find(begin, end, "--" + option);
  if (itr != end && ++itr != end)
  {
    //the option was found  
    return *itr;
  }
  return 0;
}

//check the existence of an option
bool cmd_option_exists( 
    char** begin, char** end, 
    const std::string& option
    ) 
{
  //look for the desired option
  return std::find(begin, end, "--" + option) != end;
}

//try to fill a provided variable with a command line option
template<typename T>
bool get_cmd_value(
    char ** begin, 
    char** end, 
    const std::string& option,
    T& output
    ) 
{
  if(cmd_option_exists(begin, end, option)) {
    //the option exists, get the next token
    std::stringstream ss ;
    ss << get_cmd_option(begin, end, option) ;
    //try to parse the token as the provided type
    T t ;
    ss >> t ;
    if(!ss.fail()) {
      //the parsing is allowed, fill the output
      output = t ;
      return true ;
    }
  }
  //no parsing was possible, leave the variable at its initial value
  return false ;
}

//}}}

int main(int argc, char** argv) {
  //test variables
  unsigned int dim = 20 ;
  get_cmd_value(argv, argv + argc, "dim", dim) ;

  //alea
  std::random_device seed ;
  std::mt19937_64 mt(seed()) ;
  std::uniform_real_distribution<double> random_double(-1, 1) ;

  //double matrix
  MatDX matd(dim, dim) ;
  //interval matrix
  MatIX mati(dim, dim) ;
  //rational matrix
  MatRX matr(dim, dim) ;

  //randomly fill the matrices with the same values
  for(unsigned int i = 0; i < dim; ++i) {
    for(unsigned int j = 0; j < dim; ++j) {
      double d = random_double(mt) ;
      matd(i, j) = d ;
      mati(i, j) = d ;
      matr(i, j) = d ;
    }
  }

  //compute the exact determinant in rational
  rational detr = matr.fullPivLu().determinant() ;

  std::cout << "==========" << std::endl ;
  {
    //testing fullPivLu, supposedly slower but more precise
    std::cout << "solve fullPivLu" << std::endl ;
    //double determinant
    double detd = matd.fullPivLu().determinant() ;
    try {
      //interval determinant
      interval deti = mati.fullPivLu().determinant() ;
      //computed double determinant
      std::cout << detd 
        //interval bounds
        << " in [" << deti.lower() << " -- " << deti.upper() 
        //error wrt. ground truth
        << "] error " << (double) (detd - detr)
        //width of the error interval
        << " wrt. " << deti.upper() - deti.lower() << std::endl ;
    } catch(std::exception& e) {
      //using interval yielded an exception, fallback
      std::cout << "interval computation failed" << std::endl ;
      std::cout << detd << " error " << detd - (double) detr << std::endl ;
    }
    std::cout << "==========" << std::endl ;
  }
  {
    //testing fullPivLu, supposedly faster but less precise
    std::cout << "solve partialPivLu" << std::endl ;
    double detd = matd.partialPivLu().determinant() ;
    try {
      //interval determinant
      interval deti = mati.partialPivLu().determinant() ;
      //computed double determinant
      std::cout << detd 
        //interval bounds
        << " in [" << deti.lower() << " -- " << deti.upper() 
        //error wrt. ground truth
        << "] error " << detd - (double) detr 
        //width of the error interval
        << " wrt. " << deti.upper() - deti.lower() << std::endl ;
    } catch(std::exception& e) {
      //using interval yielded an exception, fallback
      std::cout << "interval computation failed" << std::endl ;
      std::cout << detd << " error " << detd - (double) detr << std::endl ;
    }
    std::cout << "==========" << std::endl ;
  }

  return 0 ;
}
