// @licstart revoropt
// This file is part of Revoropt, a library for the computation and 
// optimization of restricted Voronoi diagrams.
//
// Copyright (C) 2013 Vincent Nivoliers <vincent.nivoliers@univ-lyon1.fr>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
// @licend revoropt

#ifndef SVGPAD_HPP
#define SVGPAD_HPP

#include <iostream>
#include <fstream>

/** tools to generate svg images **/

class SvgPad {

  public :

    /* Construction, destruction */
    SvgPad( std::ofstream& file, int width, int height) :
      file_(file), 
      width_(width), 
      height_(height), 
      fill_(), 
      stroke_(), 
      use_fill_(true), 
      use_stroke_(true) 
    {}

    /* Opening, closing */
    void open() ;

    void close() ;

    /* Drawing */
    /* integer parameters should be in [0,width]x[0,height] */
    /* double  parameters should be in [0,1]x[0,1] */
    void solid_point( int x, int y) ;
    void solid_point( double x, double y) ;

    void contour_point( int x, int y ) ;
    void contour_point( double x, double y ) ;

    void line( int x1, int y1, int x2, int y2 ) ;
    void line( double x1, double y1, double x2, double y2 ) ;

    void triangle( int x1, int y1, int x2, int y2, int x3, int y3 ) ;
    void triangle( double x1, double y1, double x2, double y2, double x3, double y3 ) ;

    void polygon( const int* vertices, unsigned int size ) ;
    void polygon( const double* vertices, unsigned int size ) ;

    void box( int minx, int miny, int maxx, int maxy ) ;
    void box( double minx, double miny, double maxx, double maxy ) ;

    /* Scaling from [0,1] to the dimension with a 5% margin */

    int scalex( double x ) ;
    int scaley( double y ) ;
    int scale_byte(double v) ;

    /* Colors */

    void fill(int r, int g, int b) ;
    void fill(const int color[3]) ;
    void fill(double r, double g, double b) ;
    void fill(const double color[3]) ;
    void toggle_fill() ;

    void stroke(int r, int g, int b) ;
    void stroke(const int color[3]) ;
    void stroke(double r, double g, double b) ;
    void stroke(const double color[3]) ;
    void toggle_stroke() ;

  private :

    /* Output file */
    std::ofstream& file_ ;

    /* Dimensions */
    int width_ ;
    int height_ ;

    /* Colors */
    int fill_[3] ;
    int stroke_[3] ;

    bool use_fill_ ;
    bool use_stroke_ ;
    void set_fill_stroke() ;
} ;

#endif
