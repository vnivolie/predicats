# Présentation sur la robustesse en géométrie algorithmique

## Résumé

Cette présentation vise à présenter sur un exemple les notions de robustesse, de
cas dégénérés, et le principe des solution de perturbation symboliques
appliquées pour résoudre ces problèmes. Les notions sont appliquées sur un code
jouet de calcul d'enveloppe convexe en 2D disponible dans le dossier `Code`. Il
est à noter que l'auteur n'est pas spécialiste du domaine, et que quelques
imprécisions ou erreurs peuvent subsister dans la présentation.

## Présentation

Les sources de la présentation sont dans le dossier `Presentation`, vous pouvez
également [télécharger le pdf compilé automatiquement](https://gitlab.liris.cnrs.fr/api/v4/projects/427/jobs/artifacts/master/raw/Presentation/presentation.pdf?job=build-presentation)

## Code jouet

La présentation modifiait le code en direct petit à petit. Ici, différentes
variantes du code sont présentées.

* [`hull.cpp`](Code/hull.cpp) : le code initial non robuste
* [`hull_perturbation.cpp`](Code/hull_perturbation.cpp) : ajout d'une perturbation dans le prédicat
* [`hull_permutation.cpp`](Code/hull_permutation.cpp) : indépendance vis à vis de l'ordre des paramètres
* [`hull_almost_exact.cpp`](Code/hull_almost_exact.cpp) : arithmétique exacte dans le prédicat

Il reste encore à modifier le prédicat pour prendre un paramètre supplémentaire,
et éviter l'appel sur des résultats intermédiaires sujets à des arrondis.

Ces codes génèrent des fichiers svg dans le dossier `/tmp`. Si vous n'êtes pas
sous linux, il vous faudra probablement changer les chemins de ces fichiers.

