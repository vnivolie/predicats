\input{beamer_preamble}

\usepackage{ucblalgo}
\setbeamercovered{invisible}

\newcommand{\vor}[1]{\ensuremath{\mathrm{Vor}(#1)}}
\newcommand{\site}{\mathbf{v}}
\newcommand{\sites}{\mathbf{V}}
\newcommand{\point}{\mathbf{p}}
\newcommand{\R}{\mathbb{R}}

\title{Robustesse des algorithmes géométriques : \\ prédicats, filtrage et perturbation}

\author{Vincent Nivoliers}

\begin{document}

\frame{
  \maketitle
}


\section{Enveloppe Convexe}

\frame {
  \frametitle{Enveloppe convexe}
  \centering
  \includegraphics[width=0.6\li]{Figures/hull}
}

\frame {
  \frametitle{1 -- trier par angle}
  \centering
  \includegraphics[width=0.6\li]{Figures/hull_sort}
}

\frame {
  \frametitle{2 -- balayage}
  \centering
  \only<1>{\includegraphics[width=0.6\li]{Figures/partial_hull}}%
  \only<2>{\includegraphics[width=0.6\li]{Figures/increment_hull_1}}%
  \only<3>{\includegraphics[width=0.6\li]{Figures/increment_hull_2}}%
  \only<4>{\includegraphics[width=0.6\li]{Figures/increment_hull_3}}%
  \only<5>{\includegraphics[width=0.6\li]{Figures/hull}}%
}

\frame {
  \frametitle{Robustesse : alignements de points}
  \centering
  \includegraphics[width=0.6\li]{Figures/hull_aligned}
}

\frame {
  \frametitle{Algorithme}
  \centering
  \begin{ucblalgo}
    \SetKwData{pt}{p}
    \SetKwData{hull}{enveloppe}
    \Algorithme{
      déterminer le point le plus à gauche \;
      \only<1>{trier les points par angle \;}%
      \only<2>{\hl{trier les points par angle} \;}
      $\hull \leftarrow$ une nouvelle pile\;
      ajouter les deux premiers points à \hull \;
      \PourCh{autre point \pt dans l'ordre}{
        $\pt_0 \leftarrow $ dernier point de l'\hull \;
        $\pt_1 \leftarrow $ avant dernier point de l'\hull \;
        \Tq{
          \only<1>{$(\pt_0\pt)$ tourne à gauche par rapport à $(\pt_1\pt)$}%
          \only<2>{\hl{$(\pt_0\pt)$ tourne à gauche par rapport à $(\pt_1\pt)$}}
        }{
          retirer le sommet de l'\hull \;
          $\pt_0 \leftarrow $ dernier point de l'\hull \;
          $\pt_1 \leftarrow $ avant dernier point de l'\hull \;
        }
        ajouter \pt à l'\hull \;
      }
      \Retour{l'\hull} \;
    }
  \end{ucblalgo}
}

\section{Perturbation}

\frame{
  \frametitle{Prédicat : orientation}
  \centering
  \includegraphics[width=0.5\li]{Figures/orient}

  \vspace{8mm}
  \begin{myblock}{0.8\li}
    \centering
    $
      \det{
        \left(
          \begin{array}{cc}
            x_0 & x_1 \\
            y_0 & y_1 \\
          \end{array}
        \right)
      }
    $ : sinus de l'angle si vecteurs normalisés.
    
    \hl{positif, négatif ou nul}
  \end{myblock}
}

\frame{
  \frametitle{Utilisation pour l'enveloppe convexe}
  \centering
  \only<1>{\includegraphics[width=0.6\li]{Figures/sort_comparison}}%
  \only<2>{\includegraphics[width=0.6\li]{Figures/sweep_comparison}}
}

\newcommand{\eps}{\ensuremath{\varepsilon}}

\frame{
  \frametitle{Perturbation}

  \begin{align*}
    \det{
      \left(
        \begin{array}{cc}
          x_0 + \eps_{0,0} & x_1 + \eps_{1,0} \\
          y_0 + \eps_{0,1} & y_1 + \eps_{1,1} \\
        \end{array}
      \right)
    }
    &= 
      \det{
        \left(
          \begin{array}{cc}
            x_0 & x_1 \\
            y_0 & y_1 \\
          \end{array}
        \right)
      } \\
    &
      + \eps_{0,0} y_1
      + \eps_{1,1} x_0
      - \eps_{0,1} x_1
      - \eps_{1,0} y_0
      \\
    &
      + \eps_{0,0}\eps_{1,1}
      - \eps_{0,1}\eps_{1,0}
  \end{align*}

  \vspace{5mm}
  \begin{myblock}{0.8\li}
    \begin{enumerate}
      \item systématiquement plus de zéro ?
      \item facilité d'évaluation ?
    \end{enumerate}
  \end{myblock}
}

\frame{
  \frametitle{Rester dans le pétrin}

  Posons que $ \forall(i,j), \eps_{i,j} = \eps > 0$

  \begin{equation*}
        \eps y_1
      + \eps x_0
      - \eps x_1
      - \eps y_0
      + \eps\eps
      - \eps\eps
  \end{equation*}

  \vspace{5mm}
  Si $(x_0,y_0) = (0,1)$ et $(x_1,y_1) = (0,2)$

  \begin{equation*}
        2\eps
      - \eps
      + \eps\eps
      - \eps\eps
      = \eps
      \quad \Rightarrow \mbox{positif !}
  \end{equation*}

  \visible<2>{
    \vspace{5mm}
    Si $(x_0,y_0) = (1,1)$ et $(x_1,y_1) = (2,2)$

    \begin{equation*}
          2\eps
        + \eps
        - 2\eps
        - \eps
        + \eps\eps
        - \eps\eps
        = 0
        \quad \Rightarrow \mbox{arf.}
    \end{equation*}
  }
}

\frame{
  \frametitle{Sortir du pétrin}

  Avec la perturbation $ \begin{pmatrix}\eps^1 & \eps^2 \\ \eps^2 & \eps^2\end{pmatrix}$

  \begin{align*}
      & \eps^1 y_1
      + \eps^2 x_0
      - \eps^2 x_1
      - \eps^2 y_0
      + \eps^1\eps^2
      - \eps^2\eps^2 \\
      &= \eps y_1 + \eps^2(x_0 - x_1 - y_0) + \eps^3 - \eps^4
  \end{align*}

  \vspace{8mm}
  \visible<2->{
    \begin{myblock}{0.8\li}
      \begin{itemize}
        \item<1-> si $y_1$ est non nul, renvoyer son signe ;
        \item<3-> si $x_0 - x_1 - y_0$ est non nul, renvoyer son signe ;
        \item<4-> sinon renvoyer positif.
      \end{itemize}
    \end{myblock}
  }
}

\frame{
  \frametitle{Calcul exact}
  \begin{center}
    \begin{myblock}{0.8\li}
      \centering
      Il peut arriver que le déterminant ne soit pas nul\\
      mais que l'arrondi en virgule flottante le soit.
    \end{myblock}
  \end{center}

  \visible<2>{
    \vspace{8mm}
    \textbf{Calculer en précision exacte :}
    \vspace{3mm}
    \begin{itemize}
      \item entiers sans limite de taille ;
      \item nombre rationels ;
      \item opérations dont le résultat est rationnel (pas de racine carrée !) ;
      \item le prédicat ne doit prendre en entrée que des nombres initiaux.
    \end{itemize}
  }
}

\frame{
  \frametitle{Généralisation de l'orientation}
  \centering
  \includegraphics[width=0.3\li]{Figures/generalized_orient}

  \vspace{8mm}
  \begin{myblock}{0.8\li}
    \centering
    examiner le signe de :
    $
      \det{
        \begin{pmatrix}
          x_0 & y_0 & 1 \\
          x_1 & y_1 & 1 \\
          x_2 & y_2 & 1
        \end{pmatrix}
      }
    $
  \end{myblock}
}

\frame{
  \frametitle{Simulation de simplicité (Edelsbrunner \& Mücke 90)}

  \begin{center}
    \begin{myblock}{0.8\li}
      Utiliser des \eps{} avec une puissance \hl{exponentielle} en $i$ et $j$ :
      \begin{equation*}
        \eps_{i,j} = \eps^{2^{i*d - j}}
      \end{equation*}
    \end{myblock}
  \end{center}

  \vspace{5mm}
  \textbf{Principe :}
  \begin{itemize}
    \item $\sum_{k=0}^n 2^k = 2^{k+1} - 1$
    \item $\prod_{k=0}^n\eps^{2^k} = \eps^{\sum_{k=0}^n2^k} = \eps^{2^{n+1} - 1} >> \eps^{2^{n+1}}$
  \end{itemize}
}

\frame{
  \frametitle{Extraire les termes}

  \begin{equation*}
    \begin{pmatrix}
      p_{0,0} + \eps_{0,0} & p_{0,1} + \eps_{0,1} & \cdots & p_{0,d} + \eps_{0,d-1} & 1 \\
      p_{1,0} + \eps_{1,0} & p_{1,1} + \eps_{1,1} & \cdots & p_{1,d} + \eps_{1,d-1} & 1 \\
      \vdots & \vdots & \ddots & \vdots \\
      p_{d,0} + \eps_{d,0} & p_{d,1} + \eps_{d,1} & \cdots & p_{d,d-1} + \eps_{d,d-1} & 1
    \end{pmatrix}
  \end{equation*}

  \vspace{5mm}
  \begin{itemize}
    \item pour chaque point aligner la perturbation voulue sur la diagonale ;
    \item développement par les cofacteurs.
  \end{itemize}
}

\end{document}
